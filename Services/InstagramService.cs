﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class InstagramService
{
    private readonly IHttpClientFactory _clientFactory;
    private readonly ILogger<InstagramService> _logger;

    public InstagramService(IHttpClientFactory clientFactory, ILogger<InstagramService> logger)
    {
        _clientFactory = clientFactory;
        _logger = logger;
    }

    public async Task<string> GetUserIdAsync(string userInfoUrl, string userName)
    {
        try
        {
            var client = _clientFactory.CreateClient();
            string userInfoJson = await client.GetStringAsync(userInfoUrl + userName);
            var userInfoJsonObject = JsonConvert.DeserializeObject<JObject>(userInfoJson);
            return (string)userInfoJsonObject["result"]["user"]["pk_id"];
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Ошибка при получении ID пользователя");
            throw;
        }
    }

    public async Task<IEnumerable<string>> GetUserStoriesAsync(string userStoriesUrl)
    {
        try
        {
            var client = _clientFactory.CreateClient();
            string userStoriesJson = await client.GetStringAsync(userStoriesUrl);
            var userStoriesObject = JsonConvert.DeserializeObject<JObject>(userStoriesJson);
            var results = (JArray)userStoriesObject["result"];
            var videoUrls = new List<string>();
            foreach (JObject item in results)
            {
                IDictionary<string, JToken> itemDictionary = item;
                if (itemDictionary.ContainsKey("video_versions"))
                {
                    JArray videoVersions = (JArray)item["video_versions"];
                    foreach (JObject video in videoVersions)
                    {
                        string videoUrl = video["url"].ToString();
                        videoUrls.Add(videoUrl);
                    }
                }
            }
            return videoUrls;
        }
        catch (Exception ex)
        {
            _logger.LogError(ex, "Ошибка при получении историй пользователя");
            throw;
        }
    }
}