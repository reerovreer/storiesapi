﻿using Microsoft.AspNetCore.Mvc;

namespace InstagramAPI.Controllers;

public class InstagramController : Controller
{
    private readonly InstagramService _instagramService;

    public InstagramController(InstagramService instagramService)
    {
        _instagramService = instagramService;
    }

    [HttpGet]
    public IActionResult Index()
    {
        return View();
    }
    [HttpPost]
    public async Task<ActionResult> Index(string userName)
    {
        string userInfoUrl = "https://igs.sf-converter.com/api/userInfoByUsername/";
        string userId = await _instagramService.GetUserIdAsync(userInfoUrl, userName);
        string userStoriesUrl = "https://igs.sf-converter.com/api/stories/" + userId;
        var videos = await _instagramService.GetUserStoriesAsync(userStoriesUrl);
        return RedirectToAction("ShowVideos", new { videoUrls = videos });
    }

    public ActionResult ShowVideos(IEnumerable<string> videoUrls)
    {
        return View(videoUrls);
    }
}